import { register, compose, withConsumer } from 'component-register';
import { withSolid, noShadowDOM } from 'solid-element';
import { onMount } from 'solid-js'

compose(
    register('test-component'),
    withSolid
)((props, { element}) => {

  onMount(async () => {
    // temporary hacks to make the SBK app play nicely with the new web components.
    // Only needed during development
    // NOT to be included in a release to prod
    console.dir(element.props);
  })

  noShadowDOM(); // no need for a Shadow DOM in this component
  return <>
    {props.children}
  </>
});
